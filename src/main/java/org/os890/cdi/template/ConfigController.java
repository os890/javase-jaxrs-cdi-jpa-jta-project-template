/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.os890.cdi.template;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;

import static javax.transaction.Transactional.TxType.REQUIRES_NEW;

@ApplicationScoped
@Transactional(REQUIRES_NEW)
public class ConfigController {
    @Inject
    private ConfigService configService;

    public List<ConfigEntry> addEntry(ConfigEntry configEntry) {
        configService.save(configEntry);
        return configService.findAll();
    }

    public List<ConfigEntry> findAll() {
        return configService.findAll();
    }

    public void cleanup() {
        findAll().forEach(configService::remove);
    }
}