/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.os890.cdi.test.weld.cdi.impl.context;

import org.os890.cdi.test.weld.cdi.TestClassScoped;

import javax.enterprise.context.spi.Contextual;
import javax.enterprise.inject.spi.BeanManager;
import java.lang.annotation.Annotation;

public class TestClassContext extends AbstractContext {
    private final ContextualStorage contextualStorage;

    public TestClassContext(BeanManager beanManager) {
        super(beanManager);
        contextualStorage = new ContextualStorage(beanManager, true, isPassivatingScope());
    }

    @Override
    protected ContextualStorage getContextualStorage(Contextual<?> contextual, boolean createIfNotExist) {
        return this.contextualStorage;
    }

    @Override
    public Class<? extends Annotation> getScope() {
        return TestClassScoped.class;
    }

    @Override
    public boolean isActive() {
        return true;
    }

    public void reset() {
        AbstractContext.destroyAllActive(this.contextualStorage);
    }
}