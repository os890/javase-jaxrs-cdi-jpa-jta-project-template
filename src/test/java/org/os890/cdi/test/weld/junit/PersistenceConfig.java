/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.os890.cdi.test.weld.junit;

import javax.enterprise.util.AnnotationLiteral;
import javax.enterprise.util.Nonbinding;
import javax.inject.Qualifier;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;

//copy of org.apache.deltaspike.jpa.api.entitymanager.PersistenceUnitName

@Target({TYPE, METHOD, PARAMETER, FIELD})
@Retention(value = RetentionPolicy.RUNTIME)
@Documented
@Qualifier
public @interface PersistenceConfig {
    @Nonbinding
    String persistenceUnitName();

    //TODO replace with config-provider
    @Nonbinding
    String dbUrl() default Literal.DEFAULT_DB_URL;

    @Nonbinding
    String dataSource() default Literal.DEFAULT_DATA_SOURCE;

    @Nonbinding
    String dataSourceUserName() default Literal.DEFAULT_USER;

    //TODO replace with credential-provider
    @Nonbinding
    String dataSourceUserPassword() default Literal.DEFAULT_PASSWORD;

    public class Literal extends AnnotationLiteral<PersistenceConfig> implements PersistenceConfig {
        private static final String DEFAULT_DB_URL = "jdbc:h2:mem:db1;DB_CLOSE_DELAY=-1";
        private static final String DEFAULT_DATA_SOURCE = "java:testDS";
        private static final String DEFAULT_USER = "sa";
        private static final String DEFAULT_PASSWORD = "";

        private final String persistenceUnitName;
        private final String dbUrl;
        private final String dataSource;
        private final String user;
        private final String password;

        public Literal(String persistenceUnitName) {
            this.persistenceUnitName = persistenceUnitName;
            this.dbUrl = DEFAULT_DB_URL;
            this.dataSource = DEFAULT_DATA_SOURCE;
            this.user = DEFAULT_USER;
            this.password = DEFAULT_PASSWORD;
        }

        @SuppressWarnings("unused")
        public Literal(String persistenceUnitName, String dbUrl, String dataSource, String user, String password) {
            this.persistenceUnitName = persistenceUnitName;
            this.dbUrl = dbUrl;
            this.dataSource = dataSource;
            this.user = user;
            this.password = password;
        }

        @Override
        public String persistenceUnitName() {
            return persistenceUnitName;
        }

        @Override
        public String dbUrl() {
            return dbUrl;
        }

        @Override
        public String dataSource() {
            return dataSource;
        }

        @Override
        public String dataSourceUserName() {
            return user;
        }

        @Override
        public String dataSourceUserPassword() {
            return password;
        }
    }
}
