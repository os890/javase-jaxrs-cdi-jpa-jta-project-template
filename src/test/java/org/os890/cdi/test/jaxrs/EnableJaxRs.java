/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.os890.cdi.test.jaxrs;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import org.junit.jupiter.api.extension.ExtendWith;
import org.os890.cdi.test.jaxrs.impl.JaxrsJunit5Extension;
import org.os890.cdi.test.weld.junit.EnableCdi;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE, ElementType.METHOD })
@Documented
@Inherited

@EnableCdi
@ExtendWith(JaxrsJunit5Extension.class)
public @interface EnableJaxRs {
    Class restResource();

    String target() default "http://localhost:%s/";

    Class<TargetProvider> targetProvider() default TargetProvider.class;

    Class[] providers() default {JacksonJsonProvider.class};

    boolean enableLogging() default false;

    boolean enableDefaultScopes() default true;

    interface TargetProvider {
        default String targetUrl() {
            return targetProtocol() + targetServer() + ":" + targetPort() + "/";
        }

        default String targetProtocol() {
            return "http://";
        }

        default String targetServer() {
            return "localhost";
        }

        default String targetPort() {
            return "%s";
        }
    }
}