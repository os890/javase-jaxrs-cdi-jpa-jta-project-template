/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.os890.cdi.test.jaxrs.impl;

import org.apache.cxf.endpoint.Server;
import org.apache.cxf.ext.logging.LoggingFeature;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.apache.cxf.jaxrs.lifecycle.SingletonResourceProvider;
import org.apache.cxf.testutil.common.TestUtil;
import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.os890.cdi.test.jaxrs.EnableJaxRs;

import javax.enterprise.inject.spi.CDI;
import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.singletonList;

public class JaxrsJunit5Extension implements BeforeAllCallback, AfterAllCallback {
    private Server server;

    public JaxrsJunit5Extension() {
    }

    @Override
    public void beforeAll(ExtensionContext context) {
        final JAXRSServerFactoryBean factory = new JAXRSServerFactoryBean();

        EnableJaxRs restConfig = context.getRequiredTestClass().getAnnotation(EnableJaxRs.class);

        final String baseUrl;
        if (EnableJaxRs.TargetProvider.class.equals(restConfig.targetProvider())) {
            baseUrl = String.format(restConfig.target(), TestUtil.getNewPortNumber(restConfig.restResource()));
        } else {
            EnableJaxRs.TargetProvider targetProvider = CDI.current().select(restConfig.targetProvider()).get();
            baseUrl = targetProvider.targetUrl();
        }
        factory.setAddress(baseUrl);

        List<Object> providers = new ArrayList<>();

        if (restConfig.enableDefaultScopes()) {
            providers.add(new InitFilter());
        }

        for (Class providerClass : restConfig.providers()) {
            providers.add(ClassUtils.tryToInstantiateClass(providerClass));
        }

        if (restConfig.enableDefaultScopes()) {
            providers.add(new FinalizerFilter());
        }

        factory.setProviders(providers);

        Class<?> resourceClass = restConfig.restResource();
        factory.setResourceClasses(resourceClass);
        factory.setResourceProvider(resourceClass, new SingletonResourceProvider(CDI.current().select(restConfig.restResource()).get(), true));

        if (restConfig.enableLogging()) {
            factory.setFeatures(singletonList(new LoggingFeature()));
        }

        CDI.current().select(UrlProducer.class).get().setBaseUrl(baseUrl);

        this.server = factory.create();
    }

    @Override
    public void afterAll(ExtensionContext context) {
        server.destroy();
    }
}
