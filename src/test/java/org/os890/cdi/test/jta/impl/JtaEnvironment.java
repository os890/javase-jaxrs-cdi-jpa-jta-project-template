/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.os890.cdi.test.jta.impl;

import com.arjuna.ats.jta.utils.JNDIManager;
import org.jnp.server.NamingBeanImpl;
import org.os890.cdi.test.util.ExceptionUtils;
import org.os890.cdi.test.weld.cdi.TestClassScoped;
import org.os890.cdi.test.weld.junit.PersistenceConfig;

@TestClassScoped
public class JtaEnvironment {
    private NamingBeanImpl namingBean;
    private static final ThreadLocal<NamingBeanImpl> ENV_TO_CLOSE = new ThreadLocal<>();

    public void start(PersistenceConfig persistenceConfig) {
        try {
            boot(persistenceConfig);
        } catch (Throwable t) {
            throw ExceptionUtils.throwAsRuntimeException(t);
        }
    }

    private void boot(PersistenceConfig persistenceConfig) throws Throwable {
        namingBean = new NamingBeanImpl();
        namingBean.start();
        ENV_TO_CLOSE.set(namingBean);

        JNDIManager.bindJTAImplementation();
        TransactionalConnectionProvider.bindDataSource(persistenceConfig);
    }

    public static void reset() {
        NamingBeanImpl namingBean = ENV_TO_CLOSE.get();
        if (namingBean != null) {
            ENV_TO_CLOSE.set(null);
            ENV_TO_CLOSE.remove();
            namingBean.stop();
        }
    }
}
