/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.os890.cdi.test.jta.impl;

import com.arjuna.ats.jdbc.TransactionalDriver;
import org.h2.jdbcx.JdbcDataSource;
import org.hibernate.engine.jdbc.connections.spi.ConnectionProvider;
import org.hibernate.service.UnknownUnwrapTypeException;
import org.os890.cdi.test.jta.PersistenceConfigBinder;
import org.os890.cdi.test.jta.PersistenceConfigDisposer;
import org.os890.cdi.test.util.ExceptionUtils;
import org.os890.cdi.test.weld.junit.PersistenceConfig;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.spi.CDI;
import javax.inject.Inject;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

@ApplicationScoped
public class TransactionalConnectionProvider implements ConnectionProvider, PersistenceConfigDisposer {
    private final TransactionalDriver transactionalDriver = new TransactionalDriver();
    private static final ThreadLocal<String> PENDING_CLEANUP_DS_NAME = new ThreadLocal<>();

    @Inject
    private ConnectionCache connectionCache;

    static void bindDataSource(PersistenceConfig persistenceConfig) {
        JdbcDataSource dataSource = new JdbcDataSource();
        dataSource.setURL(persistenceConfig.dbUrl());
        dataSource.setUser(persistenceConfig.dataSourceUserName());
        dataSource.setPassword(persistenceConfig.dataSourceUserPassword());

        PersistenceConfigBinder persistenceConfigBinder = CDI.current().select(PersistenceConfigBinder.class).get();
        persistenceConfigBinder.bind(persistenceConfig);

        try {
            InitialContext initialContext = new InitialContext();
            initialContext.bind(persistenceConfig.dataSource(), dataSource); //in case of issues we might need to use #rebind
        } catch (NamingException e) {
            throw ExceptionUtils.throwAsRuntimeException(e);
        }
    }

    @Override
    public Connection getConnection() throws SQLException {
        Connection connection = connectionCache.getActiveConnection();
        if (connection != null) {
            return connection;
        }

        Properties properties = new Properties();

        String dataSourceName = PENDING_CLEANUP_DS_NAME.get();
        String userName;
        String password;
        if (dataSourceName != null) {
            PENDING_CLEANUP_DS_NAME.set(null);
            PENDING_CLEANUP_DS_NAME.remove();

            try {
                JdbcDataSource jdbcDataSource = (JdbcDataSource) new InitialContext().lookup(dataSourceName);
                userName = jdbcDataSource.getUser();
                password = jdbcDataSource.getPassword();
            } catch (Throwable t) {
                throw ExceptionUtils.throwAsRuntimeException(t);
            }
        } else {
            PersistenceConfigBinder persistenceConfigBinder = CDI.current().select(PersistenceConfigBinder.class).get();
            dataSourceName = persistenceConfigBinder.getDataSourceName();
            userName = persistenceConfigBinder.getUserName();
            password = persistenceConfigBinder.getPassword();
        }

        properties.setProperty(TransactionalDriver.userName, userName);
        properties.setProperty(TransactionalDriver.password, password);
        connection = transactionalDriver.connect("jdbc:arjuna:" + dataSourceName, properties);
        return connectionCache.applyConnection(connection);
    }

    @Override
    public void closeConnection(Connection connection) throws SQLException {
        if (!connection.isClosed()) {
            connection.close();
        }
    }

    @Override
    public <T> T unwrap(Class<T> targetClass) {
        if (isUnwrappableAs(targetClass)) {
            return (T) this;
        }

        throw new UnknownUnwrapTypeException(targetClass);
    }

    @Override
    public boolean isUnwrappableAs(Class targetClass) {
        return getClass().isAssignableFrom(targetClass);
    }

    @Override
    public boolean supportsAggressiveRelease() {
        return false;
    }

    @Override
    public void onShutdown(String dataSourceName) {
        PENDING_CLEANUP_DS_NAME.set(dataSourceName);

        if (dataSourceName == null) {
            PENDING_CLEANUP_DS_NAME.set(null);
            PENDING_CLEANUP_DS_NAME.remove();
            JtaEnvironment.reset();
        }
    }
}
