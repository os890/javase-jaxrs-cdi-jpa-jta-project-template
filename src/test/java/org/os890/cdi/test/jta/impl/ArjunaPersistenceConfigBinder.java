/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.os890.cdi.test.jta.impl;

import org.os890.cdi.test.jta.PersistenceConfigBinder;
import org.os890.cdi.test.weld.cdi.TestClassScoped;
import org.os890.cdi.test.weld.junit.PersistenceConfig;

@TestClassScoped
public class ArjunaPersistenceConfigBinder implements PersistenceConfigBinder {
    private String activePersistenceUnitName;
    private DataSourceConfig activeDataSourceConfig;

    @Override
    public void bind(PersistenceConfig persistenceConfig) {
        this.activePersistenceUnitName = persistenceConfig.persistenceUnitName();
        this.activeDataSourceConfig = new DataSourceConfig(persistenceConfig.dataSource(), persistenceConfig.dataSourceUserName(), persistenceConfig.dataSourceUserPassword());
    }

    @Override
    public String getActivePersistenceUnitName() {
        return activePersistenceUnitName;
    }

    @Override
    public String getDataSourceName() {
        return activeDataSourceConfig.dataSourceName;
    }

    @Override
    public String getUserName() {
        return activeDataSourceConfig.userName;
    }

    @Override
    public String getPassword() {
        return activeDataSourceConfig.password;
    }

    private static class DataSourceConfig {
        private final String dataSourceName;
        private final String userName;
        private final String password;

        private DataSourceConfig(String dataSourceName, String userName, String password) {
            this.dataSourceName = dataSourceName;
            this.userName = userName;
            this.password = password;
        }
    }
}
