/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.os890.cdi.test.jta.impl;

import javax.annotation.PreDestroy;
import javax.enterprise.context.RequestScoped;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;

import static org.os890.cdi.test.util.ExceptionUtils.throwAsRuntimeException;

@RequestScoped
public class ConnectionCache implements Serializable {
    private Connection connection;

    public Connection getActiveConnection() {
        if (isActiveConnectionAvailable()) {
            return connection;
        }
        return applyConnection(null);
    }

    public Connection applyConnection(Connection connection) {
        this.connection = connection;
        return this.connection;
    }

    @PreDestroy
    protected void cleanup() {
        if (isActiveConnectionAvailable()) {
            try {
                this.connection.close();
            } catch (SQLException e) {
                throw throwAsRuntimeException(e);
            }
        }
    }

    private boolean isActiveConnectionAvailable() {
        try {
            return connection != null && !connection.isClosed() && !connection.isReadOnly();
        } catch (SQLException e) {
            throw throwAsRuntimeException(e);
        }
    }
}
