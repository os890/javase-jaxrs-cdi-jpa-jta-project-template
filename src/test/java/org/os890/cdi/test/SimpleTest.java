/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.os890.cdi.test;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.os890.cdi.template.ConfigController;
import org.os890.cdi.template.ConfigEntry;
import org.os890.cdi.template.ConfigRepository;
import org.os890.cdi.template.rest.ConfigResource;
import org.os890.cdi.template.rest.ConfigResponse;
import org.os890.cdi.test.jaxrs.EnableJaxRs;
import org.os890.cdi.test.jaxrs.TestUrl;
import org.os890.cdi.test.weld.junit.PersistenceConfig;

import javax.enterprise.context.ContextNotActiveException;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.junit.jupiter.api.Assertions.*;
import static org.os890.cdi.template.PersistenceUnitName.PROD_PU;

@EnableJaxRs(restResource = ConfigResource.class, enableLogging = false) //also starts the CDI container
@PersistenceConfig(persistenceUnitName = PROD_PU)
public class SimpleTest {
    @Inject
    private ConfigController configController;

    @Inject
    private ConfigRepository configRepository;

    @Inject
    private EntityManager entityManager;

    @Inject
    private TestUrl testUrl;

    @Test
    public void configController() {
        try {
            assertTrue(configController.findAll().isEmpty());

            configController.addEntry(new ConfigEntry("test", "demo"));

            assertFalse(configController.findAll().isEmpty());

            final Response response = ClientBuilder.newClient()
                .register(new JacksonJsonProvider())
                .target(testUrl.get())
                .path("config")
                .path("all")
                .request()
                .accept(APPLICATION_JSON)
                .get();
            Assertions.assertEquals(200, response.getStatus());
            final ConfigResponse configResponse = response.readEntity(ConfigResponse.class);

            assertEquals("test", configResponse.getConfigEntryList().iterator().next().getEntryKey());
            assertEquals("demo", configResponse.getConfigEntryList().iterator().next().getValue());
        } finally {
            configController.cleanup(); //since the TX was committed by ConfigController
        }
    }

    @Test
    public void configRepository() {
        Assertions.assertThrows(ContextNotActiveException.class, () -> configRepository.findAll());
    }

    @Test
    public void transactionScopedEntityManager() {
        Assertions.assertThrows(ContextNotActiveException.class, () -> entityManager.clear());
    }
}
